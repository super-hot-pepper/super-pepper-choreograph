<?xml version="1.0" encoding="UTF-8" ?>
<Package name="SuperPepper" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior" src="behavior_1" xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs />
    <Resources>
        <File name="test01" src="html/test01.html" />
        <File name="head" src="html/assets/sprites/head.jpg" />
        <File name="left" src="html/assets/sprites/left.jpg" />
        <File name="right" src="html/assets/sprites/right.jpg" />
        <File name="spritesheet" src="html/assets/sprites/spritesheet.png" />
        <File name="phaser.min" src="html/js/phaser.min.js" />
        <File name="epicsax" src="behavior_1/epicsax.ogg" />
        <File name="head" src="html/assets/sprites/head.png" />
        <File name="left" src="html/assets/sprites/left.png" />
        <File name="right" src="html/assets/sprites/right.png" />
        <File name="tablet_game" src="html/tablet_game.html" />
    </Resources>
    <Topics />
    <IgnoredPaths>
        <Path src="html/assets/sprites/head.png" />
        <Path src="behavior_1/epicsax.ogg" />
        <Path src="html/js/phaser.min.js" />
        <Path src="html/assets/sprites/left.jpg" />
        <Path src="html/assets/sprites/left.png" />
        <Path src="html/assets/sprites" />
        <Path src=".metadata" />
        <Path src="html/assets/sprites/right.jpg" />
        <Path src="html/assets/sprites/right.png" />
        <Path src="html" />
        <Path src="html/assets/sprites/spritesheet.png" />
        <Path src="SuperPepper.pml" />
        <Path src="manifest.xml" />
        <Path src="behavior_1/behavior.xar" />
        <Path src="html/assets" />
        <Path src="behavior_1" />
        <Path src="html/assets/sprites/head.jpg" />
        <Path src="html/test01.html" />
        <Path src="html/js" />
    </IgnoredPaths>
</Package>
